#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "hello from thread" << endl;
}

void hello_id(int id)
{
    cout << "hello from " << id << endl;
}

void print(const vector<int>& v)
{
    for (const auto &el : v)
        cout << el << ", ";
    cout << endl;
}

thread creator()
{
    return thread(hello_id, 42);
}


int main()
{
    vector<thread> threads;
    threads.push_back(thread(hello_id, 10));
    thread th(hello_id, 20);
    threads.emplace_back(move(th));
    vector<int> v{1,2,3};
    threads.emplace_back(print, cref(v));
    threads.emplace_back(creator());

    for(auto& th : threads) th.join();
    return 0;
}

