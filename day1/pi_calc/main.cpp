#include <iostream>
#include <random>
#include <thread>
#include <chrono>
#include "../../utils.h"

using namespace std;

void calc(long& counter, const long N)
{
    random_device rd;
    mt19937_64 mt(rd());
    uniform_real_distribution<> dist(-1, 1);

    for (int i = 0 ; i < N; ++i)
    {
        double x,y;
        x = dist(mt);
        y = dist(mt);
        if (x*x + y*y < 1) ++counter;
    }
}

int main()
{
    auto n_of_threads = thread::hardware_concurrency();
    cout << "cores = " << n_of_threads << endl;
    long N = 100000000;
    vector<long> counter(n_of_threads);
    auto start_t = chrono::high_resolution_clock::now();
    {
        vector<scoped_thread> thds;
        for (int i = 0 ; i < n_of_threads ; ++i)
            thds.emplace_back(calc, ref(counter[i]), N/n_of_threads);
    }
    auto end_t = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::milliseconds>(end_t-start_t).count();
    cout << " ms" << endl;

    cout << "Pi = " << double(accumulate(begin(counter), end(counter), 0L))/N * 4 << endl;

    return 0;
}

