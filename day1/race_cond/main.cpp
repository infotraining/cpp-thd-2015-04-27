#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

class spinlock
{
    std::atomic_flag flag;
public:
    spinlock() : flag(ATOMIC_FLAG_INIT) {}
    void lock()
    {
        while(flag.test_and_set());
    }

    void unlock()
    {
        flag.clear();
    }
};

volatile long counter{};
long mt_counter{};
atomic<long> at_counter{};
mutex mtx;
spinlock sl;

class guard
{
    mutex& m_;
public:
    guard(mutex& m) : m_(m)
    {
        m_.lock();
    }

    ~guard()
    {
        m_.unlock();
    }
};

void increase()
{
    for(int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<spinlock> lg(sl);
        ++counter;
    }
}

void atomic_increase()
{
    for(int i = 0 ; i < 100000 ; ++i)
        at_counter.fetch_add(1);
}

void mutex_increase()
{
    for(int i = 0 ; i < 100000 ; ++i)
    {
        lock_guard<mutex> lg(mtx);
        //mtx.lock();
        ++mt_counter;
        //mtx.unlock();
        if (mt_counter == 1000) return;
    }
}

int main()
{
    vector<scoped_thread> threads;
    auto start_t = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(increase);
    threads.clear();
    auto end_t = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_t-start_t).count();
    cout << " us" << endl;
    cout << "Counter = " << counter << endl;
    cout << "***********************" << endl;
    start_t = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(atomic_increase);
    threads.clear();
    end_t = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_t-start_t).count();
    cout << " us" << endl;
    cout << "Counter = " << at_counter.load() << endl;
    cout << "***********************" << endl;
    start_t = chrono::high_resolution_clock::now();
    for(int i = 0 ; i < 4 ; ++i)
        threads.emplace_back(mutex_increase);
    threads.clear();
    end_t = chrono::high_resolution_clock::now();
    cout << "Elapsed = ";
    cout << chrono::duration_cast<chrono::microseconds>(end_t-start_t).count();
    cout << " us" << endl;
    cout << "Counter = " << mt_counter << endl;
    cout << "***********************" << endl;

    return 0;
}

