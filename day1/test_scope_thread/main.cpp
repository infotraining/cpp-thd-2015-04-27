#include <iostream>
#include <thread>
#include <vector>
#include "../../utils.h"

using namespace std;

void hello()
{
    cout << "hello from thread" << endl;
}

void hello_id(int id)
{
    cout << "hello from " << id << endl;
}

void print(const vector<int>& v)
{
    for (const auto &el : v)
        cout << el << ", ";
    cout << endl;
}

int main()
{
    scoped_thread st1(thread(hello_id, 10));
    scoped_thread st2(hello_id, 20);
    //thread th(hello);
    //scoped_thread st2(th);
    scoped_thread st3();
    vector<scoped_thread> thds;
    thds.emplace_back(hello);
    thds.emplace_back(hello_id, 42);
    return 0;
}

