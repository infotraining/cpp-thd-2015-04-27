#include <iostream>
#include <thread>
#include <vector>

using namespace std;

void hello()
{
    cout << "hello from thread" << endl;
}

void hello_id(int id)
{
    cout << "hello from " << id << endl;
}

void increase(int& val)
{
    val++;
}

void print(const vector<int>& v)
{
    for (const auto &el : v)
        cout << el << ", ";
    cout << endl;
}

struct F
{
    void operator()(int id)
    {
        cout << "Functor " << id << endl;
    }
};

thread creator()
{
    vector<int> v{42,42,42};
    //return thread(print, v);
    return thread([v=move(v)] { print(v);});
}

class Buff
{
    vector<int> buff;
public:
    void push(const vector<int>& item)
    {
        buff.assign(item.begin(), item.end());
    }

    void print()
    {
        for (const auto &el : buff)
            cout << el << ", ";
        cout << endl;
    }
};

int main()
{
    cout << "Hello World!" << endl;
    thread th1(hello_id, 10);
    thread th2 = creator();
    int val = 20;
    thread th3(increase, ref(val));
    vector<int> v{1,2,3,4,5};
    thread th4(print, cref(v));
    F f;
    thread th5(f, 42);
    thread th6([] { cout << "lambda in thread" << endl;});
    Buff buff;

    thread th7(&Buff::push, &buff, cref(v));

    thread th8([&buff,&v] { buff.push(v);});

    // th2 = th1;
    cout << "th1 is joinable? " << th1.joinable() << endl;
    cout << "th2 is joinable? " << th2.joinable() << endl;
    cout << "After thread launch" << endl;
    th1.join();
    th2.join(); //<- error//
    th3.join();
    th4.join();
    th5.join();
    th6.join();
    th7.join();
    th8.join();
    cout << "Val = " << val << endl;
    buff.print();

    return 0;
}

