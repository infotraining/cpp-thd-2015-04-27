#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <queue>
#include <condition_variable>

using namespace std;

thread_safe_queue<int> q;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(50ms);
        q.push(i);
        cout << "produced " << i << endl;
    }
    q.push(-1);
}

void consumer(int id)
{
    for(;;)
    {
        int msg;
        q.pop(msg); // return by reference for exception safety
        if(msg == -1) {
            q.push(-1);
            return;
        }
        cout << id << " got message " << msg << endl;
    }
}

int main()
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}

