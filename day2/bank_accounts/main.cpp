#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <queue>
#include <condition_variable>

using namespace std;

class BankAccount
{
    int id;
    double balance;
    mutex mtx;
public:
    BankAccount(int id, double balance) : id(id), balance(balance)
    {
    }

    void print()
    {
        cout << "Account "<< id << " balance = " << balance << endl;
    }

    void transfer(BankAccount& to, double amount)
    {
        unique_lock<mutex> lg1(mtx, defer_lock);
        unique_lock<mutex> lg2(to.mtx, defer_lock);
        std::lock(lg1, lg2);
        balance -= amount;
        to.balance += amount;
    }

};

void test_transfers(BankAccount& from, BankAccount& to)
{
    for(int i = 0 ; i < 10000 ; ++i)
    {
        from.transfer(to, 1.0);
    }
}

int main()
{
    BankAccount acc1(1, 10000);
    BankAccount acc2(2, 10000);
    {
        scoped_thread th1(test_transfers, ref(acc1), ref(acc2));
        scoped_thread th2(test_transfers, ref(acc2), ref(acc1));
    }
    acc1.print();
    acc2.print();
    return 0;
}

