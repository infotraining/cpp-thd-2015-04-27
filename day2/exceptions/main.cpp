#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

void can_throw(int id, exception_ptr& eptr)
{
    try
    {
        cout << "Worker " << id << endl;
        this_thread::sleep_for(1s);
        if(id == 13) throw std::runtime_error("bad luck");
    }
    catch(...)
    {
        eptr = current_exception();
    }
}

int main()
{
    exception_ptr eptr;
    thread th(can_throw, 13, ref(eptr));
    th.join();
    try
    {
        if(eptr)
            rethrow_exception(eptr);
    }
    catch(std::runtime_error& err)
    {
        cerr << "error = " << err.what() << endl;
    }

    return 0;
}

