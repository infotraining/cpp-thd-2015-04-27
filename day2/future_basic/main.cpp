#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <future>

using namespace std;

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

int can_throw(int id)
{
    cout << "Worker " << id << endl;
    this_thread::sleep_for(1s);
    if(id == 13) throw std::runtime_error("bad luck");
    return 42;
}


int main()
{
    cout << "Hello World!" << endl;
    cout << question() << endl;
    future<int> res = async(launch::async, &question);
    cout << "after launching the thread" << endl;
    cout << "res = " << res.get() <<endl;
    cout << "*****************" << endl;

    future<int> res2 = async(launch::async, &can_throw, 13);
    cout << "waiting for result" << endl;
    res2.wait();
    cout << "result is ready!" << endl;
    try
    {
        cout << "res = " << res2.get() << endl;
    }
    catch(std::runtime_error& err)
    {
        cerr << "error " << err.what() << endl;
    }

    return 0;
}

