#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <queue>
#include <condition_variable>

using namespace std;

queue<int> q;
mutex mtx;
condition_variable cond;

void producer()
{
    for(int i = 0 ; i < 100 ; ++i)
    {
        this_thread::sleep_for(50ms);
        {
            lock_guard<mutex> lg(mtx);
            q.push(i);
            cond.notify_one();
        }
        cout << "produced " << i << endl;
    }
    q.push(-1);
    cond.notify_all();
}

void consumer(int id)
{
    for(;;)
    {
        unique_lock<mutex> ul(mtx);
        while (q.empty())
            cond.wait(ul);
        //cond.wait(ul, [] { return !q.empty();});
        int msg = q.front();
        if (msg == -1) return;
        q.pop();
        cout << id << " got message " << msg << endl;
    }
}

int main()
{
    scoped_thread prod(producer);
    scoped_thread cons1(consumer, 1);
    scoped_thread cons2(consumer, 2);
    return 0;
}

