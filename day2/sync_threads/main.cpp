#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>

using namespace std;

class Data
{
    vector<int> data_;
    atomic<bool> is_ready{false};
public:
    void read()
    {
        data_.resize(20);
        cout << "reading data" << endl;
        generate(data_.begin(), data_.end(), [] {return rand() % 10;});
        this_thread::sleep_for(20s);
        cout << "is atomic<bool> lock free? " << is_ready.is_lock_free() << endl;
        is_ready.store(true);
    }

    void process()
    {
        for(;;)
        {
            if(is_ready.load())
            {
                cout << "processing" << endl;
                cout << "sum = " << accumulate(data_.begin(), data_.end(), 0) << endl;
                break;
            }
            //this_thread::yield();
            this_thread::sleep_for(10us);
        }
    }
};

int main()
{
    Data data;
    scoped_thread th1(&Data::read, &data);
    scoped_thread th2(&Data::process, &data);
    scoped_thread th3(&Data::process, &data);
    scoped_thread th4(&Data::process, &data);
    scoped_thread th5(&Data::process, &data);
    return 0;
}

