#include <iostream>
#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <algorithm>
#include <queue>
#include <condition_variable>
#include <functional>

using namespace std;

/*
class Task
{
public:
    virtual void run() ;
};

class MyTask : public Task
{
public:
    void run()
    {
        do_stuff();
    }
};

thread_safe_queue<Task*> q;

q.push(new MyTask);*/

//void process_task(void(*f)())



void process_task(function<void()> f)
{
    f();
}

void jello()
{
    cout << "Jello world" << endl;
}

struct F
{
    void operator ()()
    {
        cout << "functor" << endl;
    }
};

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

int main()
{    
////    process_task(jello);
//    std::function<void()> empty;
//    //empty = jello;
//    if (empty)
//        empty();

//    F f;
////    process_task(f);
////    process_task([] () { cout << "lambada" << endl;});

    F f;
    thread_pool tp(4);
    tp.submit(jello);
    for(int i = 0 ; i < 20 ; ++i)
        tp.submit([i] () { cout << "lambada " << i << endl;
                           this_thread::sleep_for(500ms);
                         });
    tp.submit(f);
    future<int> res = tp.async(question);
    cout << "res = " << res.get() << endl;
    return 0;
}

