#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>

using namespace std;

timed_mutex mtx;

void worker_deprecated()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    for(;;)
    {
        if(mtx.try_lock())
        {
            cout << "I'v got mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(10s);
            mtx.unlock();
            return;
        }
        else
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
            this_thread::sleep_for(1s);
        }
    }
}

void worker()
{
    cout << "Worker started " << this_thread::get_id() << endl;
    unique_lock<timed_mutex> ul(mtx, try_to_lock);
    if(!ul.owns_lock())
    {
        do
        {
            cout << "Waiting for mutex " << this_thread::get_id() << endl;
        }
        while(!ul.try_lock_for(200ms));
    }
    cout << "I'v got mutex " << this_thread::get_id() << endl;
    this_thread::sleep_for(1s);
    return;
}

int main()
{
    scoped_thread th1(worker);
    scoped_thread th2(worker);
    return 0;
}

