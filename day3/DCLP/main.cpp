#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <future>
#include <fstream>
#include <string>

//myfun(unique_ptr<MyClass>(new Myclass), may_throw()); //bad
//myfun(make_unique<MyClass>(), may_throw()); //good

using namespace std;

class Service{
public:
    virtual void run() = 0;
    virtual ~Service() {};
 };

class RealService : public Service
{
    string url_;
public:
    RealService(string url) : url_("")
    {
        cout << "Creating real service" << endl;
        this_thread::sleep_for(1s);
        url_ = url;
        cout << "real service is ready" << endl;
    }

    void run() override
    {
        cout << "RealService::run" << endl;
        cout << url_ << endl;
    }
};

namespace bad {

class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    mutex mtx;

public:
    ProxyService(string url) : url_(url)
    {
    }

    void run()
    {
        if (real_service == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if (real_service == nullptr)
            {
                //real_service = (RealService*)(123);
                real_service = new RealService(url_);
            }
        }
        real_service->run();
    }

};
}

namespace atom {

class ProxyService : public Service
{
    atomic<RealService*> real_service{nullptr};
    string url_;
    mutex mtx;

public:
    ProxyService(string url) : url_(url)
    {
    }

    void run()
    {
        if (real_service.load(memory_order_relaxed) == nullptr)
        {
            lock_guard<mutex> lg(mtx);
            if (real_service.load() == nullptr)
            {
                //real_service = (RealService*)(123);
                real_service.store(new RealService(url_));
            }
        }
        (*real_service).run();
    }
};
}

namespace once_ {

class ProxyService : public Service
{
    RealService* real_service{nullptr};
    string url_;
    once_flag flag;

public:
    ProxyService(string url) : url_(url)
    {
    }

    void run()
    {
        call_once(flag, [this] { real_service = new RealService(url_);} );
        real_service->run();
    }
};
}

namespace static_ {

class ProxyService : public Service
{
    static RealService real_service; //singleton really
    string url_;
    once_flag flag;

public:
    ProxyService(string url) : url_(url)
    {
    }

    RealService& get_instance()
    {
        static RealService real_service(url_);
        return real_service;
    }

    void run()
    {
        get_instance().run();
    }
};
}

using namespace static_;

int main()
{
    ProxyService s("www.wp.pl");
    scoped_thread th1(&ProxyService::run, &s);
    this_thread::sleep_for(500ms);
    scoped_thread th2(&ProxyService::run, &s);

    return 0;
}

