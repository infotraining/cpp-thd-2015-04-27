#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <future>
#include <algorithm>

using namespace std;

template<typename It, typename T>
future<T> parallel_accumulate(It begin, It end, T init)
{
    int n_of_threads = thread::hardware_concurrency();
    auto size = distance(begin, end);
    auto block_size = size/n_of_threads;
    vector<future<T>> results;

    It block_start = begin;

    for (int i = 0 ; i < n_of_threads ; ++i)
    {
        It block_end = block_start;
        advance(block_end, block_size);
        if(i == n_of_threads-1) block_end = end;
        results.push_back(async(launch::async,
                                &accumulate<It, T>,
                                block_start,
                                block_end,
                                T()));
        block_start = block_end;
    }

    //return accumulate(results.begin(), results.end(), init,
    //                  [] (const T& b, future<T>& a) { return a.get() + b;});

    return async(launch::deferred, [init, results=move(results)] () mutable { // C++14 only
        T res = init;
        for(auto& el : results)
            res += el.get();
        return res;
    });
}

int main()
{
    vector<double> v;
    for(long i = 0 ; i < 10000000 ; ++i)
    {
        v.push_back(i);
    }

    auto start = chrono::high_resolution_clock::now();
    cout << "Sum = " << accumulate(std::begin(v), std::end(v), 0L) << endl;

    auto end = chrono::high_resolution_clock::now();

    cout << "Elapsed  = " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;

    start = chrono::high_resolution_clock::now();

    future<long> res = parallel_accumulate(std::begin(v), std::end(v), 0L);

    cout << "Sum = " << res.get() << endl;

    end = chrono::high_resolution_clock::now();

    cout << "Elapsed parallel = " << chrono::duration_cast<chrono::microseconds>(end-start).count();
    cout << " us" << endl;
    return 0;
}

