#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <future>
#include <fstream>
#include <string>

using namespace std;

class Logger
{
    ofstream f;
    thread_pool ao{1};

public:
    Logger(string fname){
        f.open(fname);
        f.exceptions ( std::ifstream::failbit | std::ifstream::badbit );
    }

    void log(string message)
    {
        ao.submit([this, message] {
            f << message << endl;
        });
    }
};

void log_data(string msg, Logger& log)
{
    for (int i = 0 ; i < 10000 ; ++i)
        log.log(msg + to_string(i));
}

int main()
{
    Logger log("test.txt");
    scoped_thread th1(log_data, "message one ", ref(log));
    scoped_thread th2(log_data, "message two ", ref(log));
    return 0;
}

