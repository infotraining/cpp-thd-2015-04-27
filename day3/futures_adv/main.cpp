#include <iostream>
#include "../../utils.h"
#include <vector>
#include <atomic>
#include <mutex>
#include <future>

using namespace std;

int question()
{
    this_thread::sleep_for(1s);
    return 42;
}

class my_packaged_task
{
    function<int()> f;
    promise<int> p;
public:
    my_packaged_task(function<int()> f) : f(f)
    {

    }
    future<int> get_future()
    {
        return p.get_future();
    }

    void operator ()()
    {
        try{
            p.set_value(f());
        }
        catch(...)
        {
            p.set_exception(current_exception());
        }
    }
};

int main()
{
    future<int> res = async(launch::async, &question);
    cout << "after launching the thread" << endl;
    cout << "res = " << res.get() <<endl;

    //packaged_task<int()> pt(question);
    my_packaged_task pt(question);
    future<int> res2 = pt.get_future();
    //function<void()> f(move(pt));
    thread(move(pt)).detach();
    //pt();
    cout << "res pt = " << res2.get() << endl;
    return 0;
}

