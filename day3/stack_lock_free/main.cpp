#include <iostream>
#include "../../utils.h"
#include <mutex>

using namespace std;

struct Node
{
    Node* next;
    int data;
};

class Stack
{
    atomic<Node*> head{nullptr};
    atomic<int> cnt{0};
public:
    void push(int data)
    {
        Node* new_node = new Node;
        new_node->data = data;

        new_node->next = head.load();
        while(!head.compare_exchange_weak(new_node->next,new_node));
    }

    int pop()
    {
        Node* old_node = head.load();
        while(!head.compare_exchange_weak(old_node, old_node->next));
        int res = old_node->data;
        delete old_node;
        return res;
    }
};

void test_stack(Stack& s)
{
    for (int i = 0 ; i < 1000 ; ++i)
    {
        s.push(i);
    }
    for (int i = 0 ; i < 1000 ; ++i)
    {
        cout << s.pop() << ", ";
    }
    cout << endl;
}

int main()
{
    Stack s;
    scoped_thread st1(test_stack, ref(s));
    scoped_thread st2(test_stack, ref(s));
    scoped_thread st3(test_stack, ref(s));
    scoped_thread st4(test_stack, ref(s));
    scoped_thread st5(test_stack, ref(s));
    return 0;
}

