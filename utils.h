#ifndef UTILS_H
#define UTILS_H

#include <thread>
#include <mutex>
#include <condition_variable>
#include <queue>
#include <vector>
#include <future>

class scoped_thread
{
    std::thread th_;
public:
//    scoped_thread() : th_()
//    {
//    }

//    scoped_thread(std::thread&& th) : th_(move(th))
//    {
//    }
// replaced by template below

    template<typename... Arg>
    scoped_thread(Arg&&... args) : th_(std::forward<Arg>(args)...)
    {

    }

    // copy ctor and cpy assignement
    scoped_thread(const scoped_thread& rval)  = delete;
    scoped_thread& operator=(const scoped_thread& rval)  = delete;

    // move ctor and assignement
    scoped_thread(scoped_thread&& rval)  = default;
    scoped_thread& operator=(scoped_thread&& rval)  = default;
//    {
//        th_ = move(rval.th_); // This is for VS2013
//    }


    ~scoped_thread()
    {
        if(th_.joinable()) th_.join();
    }
};

template<typename T>
class thread_safe_queue
{
    std::queue<T> q_;
    std::mutex mtx_;
    std::condition_variable cond_;
public:
    void push(const T& item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        q_.push(item);
        cond_.notify_one();
    }

    void push(T&& item)
    {
        std::lock_guard<std::mutex> lg(mtx_);
        q_.push(std::move(item));
        cond_.notify_one();
    }

    void pop(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx_);
        while(q_.empty())
            cond_.wait(ul);
        item = std::move(q_.front());
        q_.pop();
    }

    bool pop_nowait(T& item)
    {
        std::unique_lock<std::mutex> ul(mtx_);
        if(q_.empty())
            return false;
        item = q_.front();
        q_.pop();
        return true;
    }
};

class thread_pool
{
    using task_t = std::function<void()>;

    std::vector<scoped_thread> workers;
    thread_safe_queue<task_t> q;

    void worker()
    {
        for(;;)
        {
            task_t task;
            q.pop(task);
            if(!task) return;
            task();
        }
    }

public:
    thread_pool(int n_of_workers)
    {
        for(int i = 0 ; i < n_of_workers ; ++i)
            workers.emplace_back(&thread_pool::worker, this);
    }

    auto submit(task_t task) -> void
    {
        if(task)
            q.push(task);
    }

    template<typename F>
    auto async(F f) -> std::future<decltype(f())>
    {
        using res_t = decltype(f());
        auto pt = std::make_shared<std::packaged_task<res_t()>>(f);
        auto res = pt->get_future();
        q.push([pt] () { (*pt)();});
        return res;
    }

    ~thread_pool()
    {
        for(int i = 0 ; i < workers.size() ; ++i)
            q.push(task_t());
        workers.clear(); // join workers
    }

};

#endif // UTILS_H

